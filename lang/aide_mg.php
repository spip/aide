<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/aide-aide?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// R
	'raccourcis' => 'Hitsin-dalana an-tarehin-tsoratra',
	'raccourcis_ancre' => 'Vatofantsika voatonona anarana',
	'raccourcis_citation' => 'Sombin-tenin’ny mpanoratra',
	'raccourcis_code' => 'Kaody informatika',
	'raccourcis_glossaire' => 'Fanoroan-teny ivelany',
	'raccourcis_lien' => 'Rohy',
	'raccourcis_liste' => 'Lisitra sy fanisana',
	'raccourcis_note' => 'Fanamarihana ambany pejy',
	'raccourcis_resume' => 'Raha fintinina',
	'raccourcis_simple' => 'Fanehoana tsotra',
	'raccourcis_tableau' => 'Fafana',
];
