<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/aide-aide?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// R
	'raccourcis' => 'Типографічні скорочення',
	'raccourcis_ancre' => 'Іменовані якоря',
	'raccourcis_citation' => 'Цитата',
	'raccourcis_code' => 'Програмний код',
	'raccourcis_glossaire' => 'Зовнішній глосарій',
	'raccourcis_lien' => 'Посилання',
	'raccourcis_liste' => 'Нумерований список',
	'raccourcis_note' => 'Зноска',
	'raccourcis_resume' => 'Коротко',
	'raccourcis_simple' => 'Просте форматування',
	'raccourcis_tableau' => 'Таблиця',
];
