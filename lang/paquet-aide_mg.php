<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-aide?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// A
	'aide_description' => 'Ity plugin ity dia afaka mampiditra fanampiana voamarika kisary ao amin’i SPIP. Io fanampiana io dia azo itarina amin’ny plugins hafa.',
	'aide_nom' => 'Fanampiana SPIP',
	'aide_slogan' => 'Fanampiana an-tserasera ho an’i SPIP',
];
