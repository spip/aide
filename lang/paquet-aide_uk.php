<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-aide?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// A
	'aide_description' => 'Плагін використовується для виводу іконки з інформацією по SPIP. Можна додавати інформацію по іншим плагінам.',
	'aide_nom' => 'Довідка по SPIP',
	'aide_slogan' => 'Онлайн довідка зі SPIP',
];
