# Changelog

## 3.3.0 - 2025-11-27

## Added

- Installable en tant que package Composer

## Changed

- Compatible SPIP 5.0.0-dev

## Removed

- Drop support for PHP7.4 & PHP8.0
